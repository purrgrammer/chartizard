import { Container, Flex, Stack } from "@chakra-ui/react";

import purchases from "./data/purchases.json";
import ColorModeToggle from "./components/color-mode-toggle";
import PaymentMethodsTotals from "./components/payment-methods-totals";
import Wallets from "./components/wallets";
import PaymentMethods from "./components/payment-methods";
import Countries from "./components/countries";
import Categories from "./components/categories";
import ConfirmationTimes from "./components/confirmation-times";

function App() {
  return (
    <Container maxW="6xl" py={2}>
      <Stack gap={4}>
        <Flex justify="flex-end">
          <ColorModeToggle />
        </Flex>
        <PaymentMethodsTotals purchases={purchases} />
        <ConfirmationTimes purchases={purchases} />
        <Wallets purchases={purchases} />
        <PaymentMethods purchases={purchases} />
        <Countries purchases={purchases} />
        <Categories purchases={purchases} />
      </Stack>
    </Container>
  );
}

export default App;
