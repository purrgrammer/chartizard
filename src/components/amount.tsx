import { FormattedNumber } from "react-intl";

interface AmountProps {
  value: number;
  currency?: string;
}

export default function Amount({ value, currency = "EUR" }: AmountProps) {
  return (
    <FormattedNumber
      value={value}
      style="currency"
      currency={currency}
      minimumFractionDigits={0}
      maximumFractionDigits={2}
    />
  );
}
