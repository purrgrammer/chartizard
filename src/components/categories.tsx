import { useMemo } from "react";
import { Card, CardBody, Stack, Heading } from "@chakra-ui/react";
import groupBy from "lodash.groupby";

import Pie from "./charts/pie";
import Volume from "./volume";
import type { Purchase } from "../types";
import { getCategory } from "../purchase";

interface CategoriesProps {
  purchases: Purchase[];
}

export default function Categories({ purchases }: CategoriesProps) {
  const categories = useMemo(() => {
    return Object.entries(
      groupBy(purchases, getCategory) as Record<string, Purchase[]>,
    ).map((kv) => {
      const [category, sales] = kv;
      return { id: category, label: category, value: sales.length };
    });
  }, [purchases]);

  return (
    <Card variant="elevated">
      <CardBody>
        <Stack>
          <Heading variant="section">Categories</Heading>
          <Pie data={categories} />
          <Volume purchases={purchases} discriminator={getCategory} />
        </Stack>
      </CardBody>
    </Card>
  );
}
