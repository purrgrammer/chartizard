import { useBreakpointValue, Box } from "@chakra-ui/react";
import { ResponsiveLine } from "@nivo/line";

import useTheme from "./useTheme";

interface LineChartItem {
  x: string | number;
  y: number;
}

interface LineChartData {
  id: string;
  data: LineChartItem[];
  color?: string;
}

interface LineChartProps {
  bottomLegend: string;
  leftLegend?: string;
  colorScheme?: "nivo" | "paired";
  data: LineChartData[];
}

export default function LineChart({
  bottomLegend,
  leftLegend,
  data,
  colorScheme = "nivo",
}: LineChartProps) {
  const legendHeight = useBreakpointValue({
    base: 12,
    sm: 16,
    md: 20,
  });
  const theme = useTheme();
  const hasColors = data.every((d) => d.color);
  return (
    <Box h={{ base: "2xs", sm: "xs", md: "md" }}>
      <ResponsiveLine
        data={data}
        enableSlices="x"
        theme={theme}
        colors={hasColors ? { datum: "color" } : { scheme: colorScheme }}
        margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
        xScale={{ type: "point" }}
        yScale={{
          type: "linear",
          min: "auto",
          max: "auto",
          stacked: true,
          reverse: false,
        }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: bottomLegend,
          legendOffset: 36,
          legendPosition: "middle",
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: leftLegend,
          legendOffset: -50,
          legendPosition: "middle",
        }}
        pointColor={{ theme: "background" }}
        pointBorderWidth={2}
        pointBorderColor={{ from: "serieColor" }}
        pointLabelYOffset={-12}
        useMesh={true}
        legends={[
          {
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            translateX: 100,
            translateY: 0,
            itemsSpacing: 0,
            itemDirection: "left-to-right",
            itemWidth: 80,
            itemHeight: legendHeight ?? 20,
            itemOpacity: 0.75,
            symbolSize: 12,
            symbolShape: "circle",
            symbolBorderColor: "rgba(0, 0, 0, .5)",
          },
        ]}
      />
    </Box>
  );
}
