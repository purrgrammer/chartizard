import { useMemo } from "react";
import { Box } from "@chakra-ui/react";
import { ResponsiveChoropleth } from "@nivo/geo";

import useTheme from "./useTheme";
import features from "./world-countries.json";

interface MapItem {
  id: string;
  label: string;
  value: number;
}

export default function Map({ data }: { data: MapItem[] }) {
  const theme = useTheme();
  const maxValue = useMemo(
    () => data.reduce((acc, i) => Math.max(acc, i.value), 0),
    [data],
  );
  return (
    <Box
      h={{
        base: "xs",
        md: "md",
      }}
      w="100%"
    >
      <ResponsiveChoropleth
        theme={theme}
        features={features.features}
        data={data}
        colors="nivo"
        domain={[0, maxValue]}
        label="properties.name"
        valueFormat=".2s"
        projectionTranslation={[0.5, 0.5]}
        projectionRotation={[0, 0, 0]}
        enableGraticule={true}
        borderWidth={0.2}
        legends={[
          {
            anchor: "bottom-left",
            direction: "column",
            justify: true,
            translateX: 20,
            translateY: -100,
            itemsSpacing: 0,
            itemWidth: 94,
            itemHeight: 18,
            itemDirection: "left-to-right",
            itemOpacity: 0.85,
            symbolSize: 18,
          },
        ]}
      />
    </Box>
  );
}
