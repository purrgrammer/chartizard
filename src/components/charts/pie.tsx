import { Box } from "@chakra-ui/react";
import { ResponsivePie } from "@nivo/pie";

import useTheme from "./useTheme";

interface PieItem {
  id: string;
  label: string;
  value: number;
  color?: string;
}

interface PieGraphProps {
  data: PieItem[];
  colorScheme?: "nivo" | "paired";
}

export default function PieGraph({
  data,
  colorScheme = "nivo",
}: PieGraphProps) {
  const theme = useTheme();
  const marginX = 0;
  const marginY = 60;
  const hasColors = data.every((datum) => datum.color);
  return (
    <Box h={{ base: "2xs", sm: "xs", md: "md" }}>
      <ResponsivePie
        animate
        theme={theme}
        data={data}
        margin={{
          top: marginY,
          bottom: marginY,
          right: marginX,
          left: marginX,
        }}
        colors={hasColors ? { datum: "data.color" } : { scheme: colorScheme }}
        innerRadius={0.5}
        padAngle={0.7}
        cornerRadius={3}
        activeOuterRadiusOffset={8}
        borderWidth={1}
        borderColor={{
          from: "color",
          modifiers: [["darker", 0.2]],
        }}
        arcLinkLabelsSkipAngle={10}
        arcLinkLabelsThickness={2}
        arcLinkLabelsColor={{ from: "color" }}
        arcLabelsSkipAngle={10}
        arcLabelsTextColor={{
          from: "color",
          modifiers: [["darker", 2]],
        }}
      />
    </Box>
  );
}
