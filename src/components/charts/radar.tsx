import { Box } from "@chakra-ui/react";
import { ResponsiveRadar } from "@nivo/radar";

import useTheme from "./useTheme";

interface RadarItem {
  [key: string]: string | number;
}

interface RadarProps {
  keys: string[];
  indexBy: string;
  data: RadarItem[];
  valueFormat?: (value: number) => string;
}

export default function Radar({
  keys,
  indexBy,
  data,
  valueFormat,
}: RadarProps) {
  const theme = useTheme();
  return (
    <Box h={{ base: "3xs", sm: "xs", md:"sm" }}>
      <ResponsiveRadar
        theme={theme}
        margin={{ top: 30, bottom: 20 }}
        keys={keys}
        indexBy={indexBy}
        data={data}
        valueFormat={valueFormat}
      />
    </Box>
  );
}
