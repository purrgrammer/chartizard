import { useMemo } from "react";
import { useColorModeValue } from "@chakra-ui/react";

export default function useTheme() {
  const textColor = useColorModeValue("#333", "#DDD");
  const tooltipBackground = useColorModeValue("white", "#111");
  const theme = useMemo(() => {
    return {
      text: { fill: textColor },
      legends: { text: { fill: textColor } },
      tooltip: {
        container: {
          background: tooltipBackground,
        },
      },
    };
  }, [textColor, tooltipBackground]);
  return theme;
}
