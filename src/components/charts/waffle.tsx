import { Box, Stack, HStack, Text } from "@chakra-ui/react";
import { ResponsiveWaffle } from "@nivo/waffle";

import useTheme from "./useTheme";

interface WaffleItem {
  id: string;
  label: string;
  value: number;
  color: string;
}

interface WaffleGraphProps {
  total: number;
  data: WaffleItem[];
}

export default function WaffleGraph({ total, data }: WaffleGraphProps) {
  const theme = useTheme();

  return (
    <Stack>
      <HStack p={2} justify="center" wrap="wrap">
        {data.map((l) => (
          <HStack key={l.id}>
            <Box bg={l.color} width="16px" height="16px" borderRadius="4px" />
            <Text fontSize="sm">{l.label}</Text>
          </HStack>
        ))}
      </HStack>
      <Box h={{ base: "3xs", md: "sm" }}>
        <ResponsiveWaffle
          data={data}
          total={total}
          theme={theme}
          rows={12}
          columns={18}
          padding={1}
          margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
          colors={{ datum: "color" }}
          borderRadius={3}
          borderColor={{
            from: "color",
            modifiers: [["darker", 0.3]],
          }}
          motionStagger={2}
        />
      </Box>
    </Stack>
  );
}
