import { useMemo } from "react";
import { Card, CardBody, Stack, Heading, Text } from "@chakra-ui/react";

import Number from "./number";
import Percentage from "./percentage";
import type { Purchase } from "../types";
import { isZeroConf } from "../purchase";

export default function ConfirmationTimes({
  purchases,
}: {
  purchases: Purchase[];
}) {
  const zeroConf = useMemo(() => purchases.filter(isZeroConf), [purchases]);
  const percentage = useMemo(
    () => zeroConf.length / purchases.length,
    [zeroConf, purchases],
  );
  return (
    <Card>
      <CardBody>
        <Stack>
          <Heading variant="section">Confirmation times</Heading>
          <Text fontSize="2xl">
            <Text as="span" fontWeight="semibold">
              <Number value={zeroConf.length} />
            </Text>{" "}
            of <Number value={purchases.length} /> transactions are 0-conf,
            which makes up for{" "}
            <Text as="span" fontWeight="semibold">
              <Percentage value={percentage} />
            </Text>{" "}
            of total transactions.
          </Text>
        </Stack>
      </CardBody>
    </Card>
  );
}
