import { useMemo } from "react";
import {
  Card,
  CardBody,
  Flex,
  Stack,
  HStack,
  Heading,
  Text,
} from "@chakra-ui/react";
import groupBy from "lodash.groupby";

import Amount from "./amount";
import CountryFlag from "./country-flag";
import Map from "./charts/map";
import Radar from "./charts/radar";
import { getCountry, getWallet, getPaymentMethod, total } from "../purchase";
import { getAlphaThreeCode } from "../geo";
import { dedupe } from "../utils";
import type { Purchase } from "../types";

interface CountriesProps {
  purchases: Purchase[];
}

function BreakdownByCountry({ purchases }: CountriesProps) {
  const countries = useMemo(
    () => dedupe(purchases.map(getCountry)).filter((c) => c !== "z"),
    [purchases],
  );
  const methods = useMemo(
    () => dedupe(purchases.map(getPaymentMethod)),
    [purchases],
  );
  const wallets = useMemo(() => dedupe(purchases.map(getWallet)), [purchases]);
  const byMethods = useMemo(() => {
    return countries.map((country) => {
      const countryPurchases = purchases.filter(
        (p) => getCountry(p) === country,
      );
      return methods.reduce(
        (acc, method) => {
          const byMethod = countryPurchases.filter(
            (p) => getPaymentMethod(p) === method,
          ).length;
          acc[method] = byMethod;
          return acc;
        },
        {
          country,
        } as Record<string, string | number>,
      );
    });
  }, [countries, methods, purchases]);
  const byWallets = useMemo(() => {
    return countries.map((country) => {
      const countryPurchases = purchases.filter(
        (p) => getCountry(p) === country,
      );
      return wallets.reduce(
        (acc, wallet) => {
          const byMethod = countryPurchases.filter(
            (p) => getWallet(p) === wallet,
          ).length;
          acc[wallet] = byMethod;
          return acc;
        },
        {
          country,
        } as Record<string, string | number>,
      );
    });
  }, [countries, wallets, purchases]);
  return (
    <Stack>
      <Heading variant="section">Wallets</Heading>
      <Radar data={byWallets} indexBy="country" keys={wallets} />
      <Heading variant="section">Payment methods</Heading>
      <Radar data={byMethods} indexBy="country" keys={methods} />
    </Stack>
  );
}

export default function Countries({ purchases }: CountriesProps) {
  const byCountry = useMemo(() => {
    return Object.entries(
      groupBy(purchases, getCountry) as Record<string, Purchase[]>,
    )
      .map((kv) => {
        const [country, sales] = kv;
        const value = total(sales);
        const code = getAlphaThreeCode(country);
        return {
          id: code as string,
          label: country,
          value,
        };
      })
      .filter((i) => i.id)
      .sort((a, b) => b.value - a.value);
  }, [purchases]);

  return (
    <Card variant="elevated">
      <CardBody>
        <Stack>
          <Heading variant="section">Volume by country</Heading>
          <Flex
            gap={2}
            justify="space-between"
            direction={{
              base: "column",
              lg: "row",
            }}
          >
            <Map data={byCountry} />
            <Stack gap={1} w={{ base: "100%", lg: "sm" }}>
              <Heading variant="section" mb={3}>
                Ranking
              </Heading>
              {byCountry.map((c) => (
                <HStack key={c.id} justify="space-between">
                  <HStack>
                    <CountryFlag name={c.label} />
                    <Text>{c.label}</Text>
                  </HStack>
                  <Text color="chakra-subtle-text">
                    <Amount value={c.value} />
                  </Text>
                </HStack>
              ))}
            </Stack>
          </Flex>
          <BreakdownByCountry purchases={purchases} />
        </Stack>
      </CardBody>
    </Card>
  );
}
