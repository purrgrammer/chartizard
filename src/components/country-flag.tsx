// @ts-expect-error: No types available
import Flag from "react-world-flags";

import { getAlphaTwoCode } from "../geo";

interface CountryFlagProps {
  name: string;
  size?: number;
}

export default function CountryFlag({ name, size = 21 }: CountryFlagProps) {
  const code = getAlphaTwoCode(name);
  return code ? <Flag code={code} height={size} width={size} /> : null;
}
