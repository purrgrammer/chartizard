import { useMemo } from "react";
// @ts-expect-error: No types available
import * as Icon from "react-cryptocoins";

import { getTickerColor } from "../tickers";

export default function CurrencyIcon({
  ticker,
  size = 21,
}: {
  ticker: string;
  size?: number;
}) {
  const color = useMemo(() => getTickerColor(ticker), [ticker]);

  if (ticker === "BTC") {
    return <Icon.Btc size={size} color={color} />;
  }
  if (ticker === "Lightning") {
    return <Icon.Btc size={size} color={color} />;
  }
  if (ticker === "ETH") {
    return <Icon.Eth size={size} color={color} />;
  }
  if (ticker === "DOGE") {
    return <Icon.Doge size={size} color={color} />;
  }
  if (ticker.startsWith("USDT")) {
    return <Icon.UsdtAlt size={size} color={color} />;
  }
  if (ticker.startsWith("USTD")) {
    return <Icon.UsdtAlt size={size} color={color} />;
  }
  return null;
}
