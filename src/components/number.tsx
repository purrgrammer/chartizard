import { FormattedNumber } from "react-intl";

export default function Number({ value }: { value: number }) {
  return <FormattedNumber value={value} />;
}
