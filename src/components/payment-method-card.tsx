import {
  Card,
  CardProps,
  CardBody,
  HStack,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
} from "@chakra-ui/react";

import Amount from "./amount";
import Percentage from "./percentage";
import CurrencyIcon from "./currency-icon";

interface PaymentMethodCardProps extends CardProps {
  label: string;
  amount: number;
  percentage: number;
}

export default function PaymentMethodCard({
  label,
  amount,
  percentage,
  ...props
}: PaymentMethodCardProps) {
  return (
    <Card variant="outline" {...props}>
      <CardBody>
        <Stat>
          <HStack align="center">
            <CurrencyIcon ticker={label} />
            <StatLabel color="chakra-subtle-text">{label}</StatLabel>
          </HStack>
          <StatNumber>
            <Amount value={amount} />
          </StatNumber>
          <StatHelpText mb={0}>
            <Percentage value={percentage} />
          </StatHelpText>
        </Stat>
      </CardBody>
    </Card>
  );
}
