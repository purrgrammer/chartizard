import { useMemo } from "react";
import {
  Flex,
  Stack,
  HStack,
  Card,
  CardBody,
  Heading,
  Text,
} from "@chakra-ui/react";
import groupBy from "lodash.groupby";

import Amount from "./amount";
import PaymentMethod from "./payment-method-card";
import { total } from "../purchase";
import type { Purchase } from "../types";

export default function PaymentMethodsTotal({
  purchases,
}: {
  purchases: Purchase[];
}) {
  const totalVolume = useMemo(() => {
    return total(purchases);
  }, [purchases]);
  const paymentMethods = useMemo(() => {
    return Object.entries(
      groupBy(
        purchases,
        (p: Purchase) => p.payment_method || "unknown",
      ) as Record<string, Purchase[]>,
    )
      .map((kv) => {
        const [method, sales] = kv;
        const amount = total(sales);
        return {
          label: method,
          amount,
        };
      })
      .sort((a, b) => b.amount - a.amount);
  }, [purchases]);
  return (
    <Card>
      <CardBody>
        <Stack>
          <Heading variant="section">Volume</Heading>

          <Text fontSize="2xl">
            The total volume is{" "}
            <Text as="span" fontWeight="semibold">
              <Amount value={totalVolume} />
            </Text>{" "}
            divided as follows
          </Text>

          <Flex w="100%">
            <HStack
              spacing={4}
              wrap="nowrap"
              sx={{
                overflowX: "scroll",
                flexWrap: "nowrap",
                scrollbarWidth: "none",
                "-webkit-overflow-scrolling": "touch",
                "&::-webkit-scrollbar": {
                  display: "none",
                },
              }}
            >
              {paymentMethods.map((pm) => (
                <PaymentMethod
                  flex="0 0 auto"
                  key={pm.label}
                  label={pm.label}
                  amount={pm.amount}
                  percentage={pm.amount / totalVolume}
                />
              ))}
            </HStack>
          </Flex>
        </Stack>
      </CardBody>
    </Card>
  );
}
