import { useMemo } from "react";
import { Card, CardBody, Stack, Heading } from "@chakra-ui/react";
import { useIntl } from "react-intl";
import groupBy from "lodash.groupby";

import Waffle from "./charts/waffle";
import Line from "./charts/line";
import Volume from "./volume";
import { getPaymentMethod, getMonth } from "../purchase";
import { getTickerColor } from "../tickers";
import { Purchase } from "../types";
import { dedupe } from "../utils";

export type PaymentMethodsProps = {
  purchases: Purchase[];
};

function Trends({ purchases }: PaymentMethodsProps) {
  const { locale } = useIntl();
  const months = useMemo(
    () => dedupe(purchases.map(getMonth)).sort(),
    [purchases],
  );
  const methods = useMemo(
    () => dedupe(purchases.map(getPaymentMethod)),
    [purchases],
  );

  const purchasesTrends = useMemo(() => {
    return methods.map((method) => {
      const methodPurchases = purchases.filter(
        (p) => getPaymentMethod(p) === method,
      );
      const data = months.map((month) => {
        return {
          x: new Date(month).toLocaleString(locale, {
            month: "short",
          }),
          y: methodPurchases.filter((p) => getMonth(p) === month).length,
        };
      });

      return {
        id: method,
        color: getTickerColor(method),
        data,
      };
    });
  }, [methods]);

  return (
    <Stack>
      <Heading variant="section">Trends</Heading>
      <Line
        colorScheme="paired"
        leftLegend="purchases"
        bottomLegend="month"
        data={purchasesTrends}
      />
    </Stack>
  );
}

export default function PaymentMethods({ purchases }: PaymentMethodsProps) {
  const paymentMethods = useMemo(() => {
    return Object.entries(
      groupBy(purchases, getPaymentMethod) as Record<string, Purchase[]>,
    ).map((kv) => {
      const [method, sales] = kv;
      const color = getTickerColor(method);
      return { id: method, label: method, value: sales.length, color };
    });
  }, [purchases]);

  return (
    <Card variant="elevated">
      <CardBody>
        <Stack>
          <Heading variant="section">Payment methods</Heading>
          <Waffle data={paymentMethods} total={purchases.length} />
          <Trends purchases={purchases} />
          <Volume purchases={purchases} discriminator={getPaymentMethod} />
        </Stack>
      </CardBody>
    </Card>
  );
}
