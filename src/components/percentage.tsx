import { FormattedNumber } from "react-intl";

export default function Percentage({ value }: { value: number }) {
  return (
    <FormattedNumber
      value={value}
      style="percent"
      minimumFractionDigits={0}
      maximumFractionDigits={2}
    />
  );
}
