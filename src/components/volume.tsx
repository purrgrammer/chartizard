import { useMemo } from "react";
import { Stack, Heading } from "@chakra-ui/react";
import { useIntl } from "react-intl";

import Radar from "./charts/radar";
import type { Purchase } from "../types";
import { total } from "../purchase";
import { dedupe } from "../utils";

interface VolumeProps {
  purchases: Purchase[];
  discriminator(p: Purchase): string;
}

export default function Volume({ purchases, discriminator }: VolumeProps) {
  const { formatNumber } = useIntl();
  const ids = useMemo(() => {
    return dedupe(purchases.map(discriminator));
  }, [purchases]);
  const byIds = useMemo(() => {
    return ids.map((id) => {
      const idPurchases = purchases.filter((p) => discriminator(p) === id);
      const volume = total(idPurchases);
      return {
        id,
        volume,
      };
    });
  }, [ids, purchases]);
  return (
    <Stack>
      <Heading variant="section">Volume</Heading>
      <Radar
        data={byIds}
        indexBy="id"
        keys={["volume"]}
        valueFormat={(value) => {
          return formatNumber(value, {
            style: "currency",
            currency: "EUR",
            minimumFractionDigits: 0,
            maximumFractionDigits: 2,
          });
        }}
      />
    </Stack>
  );
}
