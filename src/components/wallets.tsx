import { useMemo } from "react";
import { Card, CardBody, Stack, Heading } from "@chakra-ui/react";
import { useIntl } from "react-intl";
import groupBy from "lodash.groupby";

import Pie from "./charts/pie";
import Line from "./charts/line";
import Volume from "./volume";
import { getWallet, getMonth } from "../purchase";
import { dedupe } from "../utils";
import { Purchase } from "../types";

export type WalletsProps = {
  purchases: Purchase[];
};

function Trends({ purchases }: WalletsProps) {
  const { locale } = useIntl();
  const months = useMemo(
    () => dedupe(purchases.map(getMonth)).sort(),
    [purchases],
  );
  const wallets = useMemo(() => dedupe(purchases.map(getWallet)), [purchases]);

  const trends = useMemo(() => {
    return wallets.map((wallet) => {
      const walletPurchases = purchases.filter((p) => getWallet(p) === wallet);
      const data = months.map((month) => {
        return {
          x: new Date(month).toLocaleString(locale, {
            month: "short",
          }),
          y: walletPurchases.filter((p) => getMonth(p) === month).length,
        };
      });

      return {
        id: wallet,
        data,
      };
    });
  }, [wallets]);

  return (
    <Stack>
      <Heading variant="section">Trends</Heading>
      <Line
        colorScheme="paired"
        leftLegend="purchases"
        bottomLegend="month"
        data={trends}
      />
    </Stack>
  );
}

export default function Wallets({ purchases }: WalletsProps) {
  const wallets = useMemo(() => {
    return Object.entries(
      groupBy(purchases, getWallet) as Record<string, Purchase[]>,
    ).map((kv) => {
      const [wallet, sales] = kv;
      return {
        id: wallet,
        label: wallet,
        value: sales.length,
      };
    });
  }, [purchases]);

  return (
    <Card variant="elevated">
      <CardBody>
        <Stack>
          <Heading variant="section">Wallets</Heading>
          <Pie colorScheme="paired" data={wallets} />
          <Trends purchases={purchases} />
          <Volume purchases={purchases} discriminator={getWallet} />
        </Stack>
      </CardBody>
    </Card>
  );
}
