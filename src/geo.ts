import countries from "world-countries";

export function getAlphaTwoCode(name: string) {
  const commonName = name.toLowerCase();
  const country = countries.find(
    (c) =>
      c.name.common.toLowerCase() === commonName ||
      c.altSpellings.includes(name),
  );
  return country && country.cca2;
}

export function getAlphaThreeCode(name: string) {
  const commonName = name.toLowerCase();
  const country = countries.find(
    (c) =>
      c.name.common.toLowerCase() === commonName ||
      c.altSpellings.includes(name),
  );
  return country && country.cca3;
}
