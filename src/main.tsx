import React from "react";
import ReactDOM from "react-dom/client";
import { ChakraProvider, ColorModeScript } from "@chakra-ui/react";
import { IntlProvider } from "react-intl";

import { theme } from "./theme";
import App from "./App.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <IntlProvider locale="en">
        <ColorModeScript initialColorMode={theme.config.initialColorMode} />
        <App />
      </IntlProvider>
    </ChakraProvider>
  </React.StrictMode>,
);
