import type { Purchase, PaymentMethod, PurchaseCategory } from "./types";

export function getWallet(p: Purchase): string {
  return p.user_wallet || "Unknown";
}

export function getPaymentMethod(p: Purchase): PaymentMethod {
  return p.payment_method || "Unknown";
}

export function getPrice(p: Purchase): number {
  return p.euro_price ?? 0;
}

export function getCategory(p: Purchase): PurchaseCategory {
  return p.purchase_category;
}

export function getCountry(p: Purchase): string {
  return p.country;
}

export function isZeroConf(p: Purchase): boolean {
  return Boolean(p.zero_conf_time) || p.time_to_onchain_conf === 0;
}

export function getTime(p: Purchase): string {
  return p.created_time;
}

export function getMonth(p: Purchase): string {
  const [year, month] = p.created_time.split("-");
  return `${year}-${month}`;
}

export function total(ps: Purchase[]): number {
  return ps.reduce((acc, p) => acc + getPrice(p), 0);
}
