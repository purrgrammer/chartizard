import { extendTheme } from "@chakra-ui/react";
import { defineStyle } from "@chakra-ui/react";

import "@fontsource/inter/400.css";
import "@fontsource/inter/600.css";
import "@fontsource/inter/700.css";

// Color mode

const config = {
  initialColorMode: "light",
  useSystemColorMode: false,
};

// Typography

const fonts = {
  heading: `'Inter', sans-serif`,
  body: `'Inter', sans-serif`,
};

// Styles

const styles = {
  global: {
    body: {
      bg: "#E8E9F6",
      _dark: {
        bg: "#111",
      },
    },
  },
};

// Components

const section = defineStyle({
  color: "chakra-subtle-text",
  fontSize: "xs",
  fontWeight: 600,
  textTransform: "uppercase",
});

const headingTheme = { variants: { section } };

// Theme

export const theme = extendTheme({
  config,
  fonts,
  styles,
  components: {
    Heading: headingTheme,
  },
});
