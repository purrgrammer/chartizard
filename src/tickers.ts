export function getTickerColor(ticker: string): string {
  if (ticker === "BTC") {
    return "orange";
  }
  if (ticker === "Lightning") {
    return "#bda800";
  }
  if (ticker === "ETH") {
    return "#7979DE";
  }
  if (ticker === "DOGE") {
    return "#cb9800";
  }
  if (ticker.startsWith("USDT")) {
    return "#26A17B";
  }
  if (ticker.startsWith("USTD")) {
    return "#85BB65";
  }
  return "gray";
}

