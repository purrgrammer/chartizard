export type PaymentMethod = string;

export type PurchaseCategory = string;

export interface Purchase {
  id: number;
  payment_method: PaymentMethod;
  purchase_category: PurchaseCategory;
  created_time: string;
  zero_conf_time: string | null;
  time_to_onchain_conf: number | null;
  euro_price: number;
  satoshi_amount: number;
  fee_rate: number;
  is_from_exchange: 0 | 1;
  has_account: 0 | 1;
  is_walletconnect: "Y" | "N";
  fee_estimates: number | null;
  exchange_name: string | null;
  user_wallet: string | null;
  country: string;
}
